	
%bcond_with lint
%bcond_without tests

%global __python %{__python3}
%global python_sitelib %{python3_sitelib}

Summary: Builds packages inside chroots
Name: mock
Version: 5.6
Release: 2
License: GPL-2.0-or-later
Source:  https://github.com/rpm-software-management/mock/releases/download/mock-2.2-1/%{name}-%{version}.tar.gz
URL: https://github.com/rpm-software-management/mock/
BuildArch: noarch
BuildRequires: bash-completion
Requires: python3-distro
Requires: python3-jinja2
Requires: python3-six >= 1.4.0
Requires: python3-requests
Requires: python3-rpm
Requires: python3-pyroute2
BuildRequires: python3-devel
BuildRequires: python3-backoff
%if %{with lint}
BuildRequires: python3-pylint
%endif
BuildRequires: python3-rpm
BuildRequires: python3-rpmautospec-core

Requires: tar
Requires: pigz
Requires: usermode
Requires: createrepo_c

# We know that the current version of mock isn't compatible with older variants,
# and we want to enforce automatic upgrades.
 
Requires: mock-core-configs

Requires: systemd
Requires: systemd-container
Requires: coreutils
Requires: util-linux
Requires: coreutils
Requires: procps-ng
Requires: dnf
Requires: dnf-plugins-core

Recommends: python3-dnf
Recommends: python3-dnf-plugins-core
 
# YUM stack, dnf-utils replace yum-utils
Recommends: yum
Recommends: dnf-utils
 
Recommends: btrfs-progs
Suggests: qemu-user-static
Suggests: procenv
Recommends: podman

%if %{with tests}
BuildRequires: python3-distro
BuildRequires: python3-jinja2
BuildRequires: python3-pyroute2
BuildRequires: python3-pytest
BuildRequires: python3-requests
BuildRequires: python3-templated-dictionary
%endif

%description
Mock takes an SRPM and builds it in a chroot.

%package scm
Summary: Mock SCM integration module
Requires: %{name} = %{version}-%{release}
Recommends: cvs
Recommends: git
Recommends: subversion
Recommends: tar
 
# We could migrate to 'copr-distgit-client'
Recommends: rpkg
 
%description scm
Mock SCM integration module.
 
%package lvm
Summary: LVM plugin for mock
Requires: %{name} = %{version}-%{release}
Requires: lvm2
 
%description lvm
Mock plugin that enables using LVM as a backend and support creating snapshots
of the buildroot.
 
%package rpmautospec
Summary: Rpmautospec plugin for mock
Requires: %{name} = %{version}-%{release}
# This lets mock determine if a spec file needs to be processed with rpmautospec.
Requires: python%{python3_pkgversion}-rpmautospec-core
 
%description rpmautospec
Mock plugin that preprocesses spec files using rpmautospec.

%package filesystem
Summary:  Mock filesystem layout
Requires(pre):  shadow-utils
 
%description filesystem
Filesystem layout and group for Mock.

%prep
%setup -q
for file in py/mock.py py/mock-parse-buildlog.py; do
  sed -i 1"s|#!/usr/bin/python3 |#!%{__python} |" $file
done

%build
for i in py/mockbuild/constants.py py/mock-parse-buildlog.py; do
    perl -p -i -e 's|^VERSION\s*=.*|VERSION="%{version}"|' $i
    perl -p -i -e 's|^SYSCONFDIR\s*=.*|SYSCONFDIR="%{_sysconfdir}"|' $i
    perl -p -i -e 's|^PYTHONDIR\s*=.*|PYTHONDIR="%{python_sitelib}"|' $i
    perl -p -i -e 's|^PKGPYTHONDIR\s*=.*|PKGPYTHONDIR="%{python_sitelib}/mockbuild"|' $i
done
for i in docs/mock.1 docs/mock-parse-buildlog.1; do
    perl -p -i -e 's|\@VERSION\@|%{version}"|' $i
done

%install
mkdir -p %{buildroot}%{_sysconfdir}/mock/eol/templates
mkdir -p %{buildroot}%{_sysconfdir}/mock/templates

install -d %{buildroot}%{_bindir}
install -d %{buildroot}%{_libexecdir}/mock
install mockchain %{buildroot}%{_bindir}/mockchain
install py/mock-parse-buildlog.py %{buildroot}%{_bindir}/mock-parse-buildlog
install py/mock.py %{buildroot}%{_libexecdir}/mock/mock
ln -s consolehelper %{buildroot}%{_bindir}/mock
install create_default_route_in_container.sh %{buildroot}%{_libexecdir}/mock/
 
install -d %{buildroot}%{_sysconfdir}/pam.d
cp -a etc/pam/* %{buildroot}%{_sysconfdir}/pam.d/

install -d %{buildroot}%{_sysconfdir}/mock
cp -a etc/mock/* %{buildroot}%{_sysconfdir}/mock/

install -d %{buildroot}%{_sysconfdir}/security/console.apps/
cp -a etc/consolehelper/mock %{buildroot}%{_sysconfdir}/security/console.apps/%{name}

install -d %{buildroot}%{_datadir}/bash-completion/completions/
cp -a etc/bash_completion.d/* %{buildroot}%{_datadir}/bash-completion/completions/
cp -a etc/bash_completion.d/* %{buildroot}%{_datadir}/bash-completion/completions/
ln -s mock %{buildroot}%{_datadir}/bash-completion/completions/mock-parse-buildlog

install -d %{buildroot}%{_sysconfdir}/pki/mock
cp -a etc/pki/* %{buildroot}%{_sysconfdir}/pki/mock/

install -d %{buildroot}%{python_sitelib}/
cp -a py/mockbuild %{buildroot}%{python_sitelib}/

install -d %{buildroot}%{_mandir}/man1
cp -a docs/mock.1 docs/mock-parse-buildlog.1 %{buildroot}%{_mandir}/man1/
install -d %{buildroot}%{_datadir}/cheat
cp -a docs/mock.cheat %{buildroot}%{_datadir}/cheat/mock

install -d %{buildroot}/var/lib/mock
install -d %{buildroot}/var/cache/mock

	
mkdir -p %{buildroot}%{_pkgdocdir}
install -p -m 0644 docs/site-defaults.cfg %{buildroot}%{_pkgdocdir}
 
sed -i 's/^_MOCK_NVR = None$/_MOCK_NVR = "%name-%version-%release"/' \
    %{buildroot}%{_libexecdir}/mock/mock

%check
%if %{with lint}
# ignore the errors for now, just print them and hopefully somebody will fix it one day
pylint-3 py/mockbuild/ py/*.py py/mockbuild/plugins/* || :
%endif
	
%if %{with tests}
./run-tests.sh --no-cov
%endif

%files
%defattr(0644, root, mock)
%doc %{_pkgdocdir}/site-defaults.cfg
%{_datadir}/bash-completion/completions/mock
%{_datadir}/bash-completion/completions/mock-parse-buildlog

%defattr(-, root, root)
%{_bindir}/mockchain
%{_bindir}/mock
%{_bindir}/mock-parse-buildlog
%{_libexecdir}/mock

%{python_sitelib}/*
%exclude %{python_sitelib}/mockbuild/scm.*
%exclude %{python_sitelib}/mockbuild/__pycache__/scm.*
%exclude %{python_sitelib}/mockbuild/plugins/lvm_root.*
%exclude %{python_sitelib}/mockbuild/plugins/__pycache__/lvm_root.*

# config files
%config(noreplace) %{_sysconfdir}/%{name}/*.ini
%config(noreplace) %{_sysconfdir}/pam.d/%{name}
%config(noreplace) %{_sysconfdir}/security/console.apps/%{name}

%dir %{_sysconfdir}/pki/mock
%config(noreplace) %{_sysconfdir}/pki/mock/*

%{_mandir}/man1/mock.1*
%{_mandir}/man1/mock-parse-buildlog.1*
%{_datadir}/cheat/mock

%defattr(0775, root, mock, 0775)
%dir %{_localstatedir}/cache/mock
%dir %{_localstatedir}/lib/mock

	
%files scm
%{python_sitelib}/mockbuild/scm.py*
%{python3_sitelib}/mockbuild/__pycache__/scm.*.py*
 
%files lvm
%{python_sitelib}/mockbuild/plugins/lvm_root.*
%{python3_sitelib}/mockbuild/plugins/__pycache__/lvm_root.*.py*

%files rpmautospec
%{python_sitelib}/mockbuild/plugins/rpmautospec.*
%{python3_sitelib}/mockbuild/plugins/__pycache__/rpmautospec.*.py*

%files filesystem
%license COPYING
%dir  %{_sysconfdir}/mock
%dir  %{_sysconfdir}/mock/eol
%dir  %{_sysconfdir}/mock/eol/templates
%dir  %{_sysconfdir}/mock/templates
%dir  %{_datadir}/cheat

%changelog
* Sun Oct 20 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 5.6-2
- update check

* Thu Apr 25 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 5.6-1
- update to 5.6

* Thu Nov 10 2022 xu_ping <xuping33@h-partners.com> 2.2-2
- fix source url

* Fri May 15 2020 hexiaowen <hexiaowen@huawei.com> 2.2-1
- first build
